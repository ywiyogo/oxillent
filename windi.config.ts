import { defineConfig } from 'windicss/helpers'

export default defineConfig({
  preflight: false,
  theme: {
    extend: {
      fontFamily: {
        archivo: ['Archivo', 'sans-serif'],
      },
      colors: {
        oxigreen: '#1AFD00',
        oxiblue: '#0004b9'
      },
      lineHeight: {
        '7': '2rem',
        '8': '3.5rem'
      },
      width: {
        '200px': '200px',
        '1000px': '1000px'
      },
      height: {
        '70px': '70px',
        '400px': '400px',
        '2500px': '2500px'
      },
      fontSize: {
        oxi_xs: '10pt',
        oxi_s: '14pt',
        oxi_m: '18pt',
        oxi_l: '24pt',
        oxi_lg: '48pt'
      },
    },
  },
})
