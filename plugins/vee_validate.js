import { extend, localize } from 'vee-validate'
import { required, email, min } from 'vee-validate/dist/rules'
extend('email', email)
extend('min', min)
extend('required', {
  ...required,
  message: 'Das Feld ist erforderlich',
})
import de from 'vee-validate/dist/locale/de.json'

// Install and Activate the locale.
localize('de', de)
